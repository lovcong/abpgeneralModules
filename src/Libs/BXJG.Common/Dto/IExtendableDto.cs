﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.Common.Dto
{
    public interface IExtendableDto
    {
        public dynamic ExtensionData { get; set; }
    }
}
