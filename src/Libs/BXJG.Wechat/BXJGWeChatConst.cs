﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.WeChat
{
    /// <summary>
    /// 微信模块公共常量
    /// </summary>
    public class BXJGWeChatConst
    {
        /// <summary>
        /// 微信模块根配置节点名，appsettings中微信的配置节点名称
        /// </summary>
        public const string RootConfigKey = "bxjgWeChat";
    }
}
