﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BXJG.CMS.Ad
{
    /// <summary>
    /// 广告控件类型
    /// </summary>
    public enum AdControlType
    {
        /// <summary>
        /// 图片展示控件
        /// </summary>
        Image,
        /// <summary>
        /// 文本展示控件
        /// </summary>
        Text,
        /// <summary>
        /// Html展示控件
        /// </summary>
        Html,
        /// <summary>
        /// 轮播图展示控件
        /// </summary>
        Rotation
    }
}
