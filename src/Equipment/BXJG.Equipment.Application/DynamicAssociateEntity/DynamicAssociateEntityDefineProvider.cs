﻿using BXJG.DynamicAssociateEntity;
using BXJG.Equipment.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.Equipment.DynamicAssociateEntity
{
    public class DynamicAssociateEntityDefineProvider : IDynamicAssociateEntityDefineProvider
    {
        public IEnumerable<DynamicAssociateEntityDefine> GetDefines(DynamicAssociateEntityDefineProviderContext context)
        {
            return new DynamicAssociateEntityDefine[]
            {
                new DynamicAssociateEntityDefine
                {
                    Name="equipment",
                    DisplayName = "设备".BXJGEquipmentL(),
                    ServiceType= typeof(DynamicAssociateEntityEquipmentInfoService),
                    NeedPagination=true,
                    Fields= new DynamicAssociateEntityDefineField[]
                    {
                        new DynamicAssociateEntityDefineField
                        {
                            DislayName="id".BXJGEquipmentL(),
                            IsKey=true,
                            Name="id"
                        },
                        new DynamicAssociateEntityDefineField
                        {
                            DislayName ="设备名称".BXJGEquipmentL(),
                            IsDisplayField = true,
                            Name = "name"
                        },
                        new DynamicAssociateEntityDefineField
                        {
                            DislayName = "硬件码".BXJGEquipmentL(),
                            IsDisplayField = true,
                            Name ="hardwareCode"
                        }
                    }
                }
            };
        }
    }
}
