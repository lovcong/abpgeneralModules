﻿using BXJG.GeneralTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.GoodsInfo.Application.Admin
{
    /// <summary>
    /// 后台管理物品分类移动节点时的输入模型
    /// </summary>
    public class GoodsInfoCategoryMoveInput : GeneralTreeNodeMoveInput
    {
    }
}
