﻿using BXJG.GeneralTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.GoodsInfo.Application.Admin
{
    /// <summary>
    /// 后台管理物品分类的显示模型
    /// </summary>
    public class GoodsInfoCategoryDto : GeneralTreeGetTreeNodeBaseDto<GoodsInfoCategoryDto>
    {
    }
}
