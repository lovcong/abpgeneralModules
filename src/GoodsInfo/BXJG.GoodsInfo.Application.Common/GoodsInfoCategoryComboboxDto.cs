﻿using BXJG.GeneralTree;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BXJG.GoodsInfo.Application.Common
{
    /// <summary>
    /// 获取物品分类扁平化结构的下拉框数据模型
    /// </summary>
    public class GoodsInfoCategoryComboboxDto: GeneralTreeComboboxDto
    {
    }
}
